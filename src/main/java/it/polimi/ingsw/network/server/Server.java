package it.polimi.ingsw.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private int port;
	private ServerSocket serverSocket;

	public Server(int port) {
		this.port = port;
	}

	/*
	 * Fa partire il server
	 */
	public void startServer() {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Ready");

			while (isStopped()) {
				// in attesa di una nuova conessione, appena si connette si crea
				// una socket relativa al client appena connesso.
				Socket socket = serverSocket.accept();
				// per gestire più connessioni in parallelo creo un thread
				new ClientHandler(new SocketComunicator(socket)).start();;
			}

		} catch (IOException e) {
			// eccezione se non possiedo l'ip, se la porta non è collegata

			System.err.println("Cannot connect!");
		}
	}

	/*
	 * il server è fermo o meno, va controllato nel progetto
	 */
	public boolean isStopped() {
		return true;
	}

	public static void main(String[] args) {

		Server server = new Server(27222);
		server.startServer();
	}

}
