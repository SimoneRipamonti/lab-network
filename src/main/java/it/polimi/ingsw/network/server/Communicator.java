package it.polimi.ingsw.network.server;

public interface Communicator {
	// invio
	void send(String msg);
	// ricezione
	String receive();
	
	// chiusura
	void close();
}
