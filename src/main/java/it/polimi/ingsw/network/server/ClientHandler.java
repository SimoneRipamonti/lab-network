package it.polimi.ingsw.network.server;

public class ClientHandler extends Thread {

	private Communicator c;
	
	// costruttore
	// comunicator ha tutti i comandi del cliente
	public ClientHandler(Communicator c) {
		this.c = c;
	}

	// override di run di thread
	@Override
	public void run() {
		// comando utente
		String command = "";
		// controllo il comando utente con un ciclo do while, finche non ho "exit"
		do {
			command = c.receive();
			// logica secondo il valore di comand
			c.send("Receive: "+command);
			
		} while (!command.equals("exit"));
	}
}
