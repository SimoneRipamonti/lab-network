package it.polimi.ingsw.network.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SocketComunicator implements Communicator {
	// implemento tutti i metodi di communicator
	private Scanner in;
	private Socket socket;
	private PrintWriter out;

	public SocketComunicator(Socket s) {
		this.socket = s;
		try {
			in = new Scanner(socket.getInputStream());
			out = new PrintWriter(socket.getOutputStream());
		} catch (IOException e) {
			// nel caso ho un errore nello scriver
			System.err.println("Error on stream");
		}
	}

	public void send(String msg) {
		// invio di messaggi
		out.println(msg);
		out.flush(); // invia il messaggio senza bufferizzare
	}

	public String receive() {
		// ricezione
		return in.nextLine();
	}

	public void close() {
		// chiusura della comunicazione, della socket, chiudendo tutti gli
		// stream associati alla socket
		try {
			socket.close();
		} catch (IOException e) {
			System.err.println("Cannot close client connection");
		} finally{
			// se non riesco a chiuderla
			socket = null;
		}
	}

}
