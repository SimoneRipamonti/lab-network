package it.polimi.ingsw.network.client;

import it.polimi.ingsw.network.server.SocketComunicator;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	// ip e porta del server
	private int port;
	private String ip;

	public Client(String ip, int port){
		this.ip = ip;
		this.port = port;
	}
	
	public void startClient(){
		
		// nuova socket per connettermi al server
		try {
			String command ="";
			Scanner in = new Scanner(System.in);
			Socket socket = new Socket(ip, port);
			SocketComunicator server = new SocketComunicator(socket);
			
			do{
				command = in.nextLine();
				server.send(command);
				String msg = server.receive();
				System.out.println("Received response: "+msg);
			}while(!command.equals("exit"));
			
			// chiudere connessione
			server.close();
			in.close();
			
			
		} catch (UnknownHostException e) {
			System.err.println("Unknown host");
		} catch (IOException e) {
			System.err.println("Error");
		}
	}
	
	public static void main(String[] args) {
		Client client = new Client("127.0.0.1", 27222);
		client.startClient();
	}

}
