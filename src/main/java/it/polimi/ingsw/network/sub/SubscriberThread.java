package it.polimi.ingsw.network.sub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class SubscriberThread extends Thread{

	private int id;
	private int port = 7777;
	private String address ="127.0.0.1";
	private Socket subSocket;
	private BufferedReader in;
	
	public SubscriberThread(int id){
		this.id = id;
		subscribe();
	}

	private void subscribe() {
		try {
			subSocket = new Socket(address, port);
			// InputStreamReader va bene per stringhe di byte
			in = new BufferedReader(new InputStreamReader(subSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Cannot connect to broker");
		} catch (IOException e) {
			System.err.println("Cannot connect to broker");
		}
	}
	
	@Override
	public void run(){
		// looppo perchè voglio restare sempre in ascolto
		while(true){
			receive();
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				System.err.println("Cannot sleep");
			} // per tenere i cicli di cpu bassi
		}
	}

	private void receive() {
		String msg = null;
		try {
			msg = in.readLine();
			if(msg!=null){
				// mia logica
				System.out.println("Thread-"+id+": "+msg);
			}
		} catch (IOException e) {
			System.err.println("Cannot read from input stream!");
		}
	}
}
