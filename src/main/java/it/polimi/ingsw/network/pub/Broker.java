package it.polimi.ingsw.network.pub;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Scanner;

public class Broker extends Thread {

	private String topic;
	private int portNumber = 7777;

	// non serve concorrente perchè un solo thread parla con la lista
	private ArrayList<BrokerThread> subscribers = new ArrayList<BrokerThread>();

	public Broker(String topic) {
		this.topic = topic;
	}

	public static void main(String[] args) {
		System.out.println("Starting broker");
		Broker broker = new Broker("Game1");
		broker.start();

		Scanner in = new Scanner(System.in);
		while (true) {
			String msg = in.nextLine();
			broker.publish(msg);
		}

	}

	@Override
	public void run() {
		// metto il server in ascolto di connessioni in ingresso
		try (ServerSocket brokerSocket = new ServerSocket(portNumber)) {
			// creo un nuovo thread con la nuova connessione
			while (true) {
				BrokerThread brokerThread = new BrokerThread(
						brokerSocket.accept());
				brokerThread.start();
				subscribers.add(brokerThread);
			}
		} catch (IOException e) {
			System.err.println("Cannot listen to port " + portNumber);
			System.exit(-1);
		}
	}

	public void publish(String msg) {
		if (!subscribers.isEmpty()) {
			System.out.println("publishing message");
			for (BrokerThread sub : subscribers) {
				sub.dispatchMessage(msg);

			}
		} else {
			System.err.println("No subscribers");
		}
	}
}
