package it.polimi.ingsw.network.pub;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BrokerThread extends Thread {

	private Socket socket;
	private PrintWriter out;
	private ConcurrentLinkedQueue<String> buffer;

	public BrokerThread(Socket socket) {
		this.socket = socket;
		buffer = new ConcurrentLinkedQueue<String>();
		try {
			out = new PrintWriter(socket.getOutputStream());
		} catch (IOException e) {
			System.err.println("Cannot connect to subscriber");
		}
	}

	public void send(String msg) {
		out.println(msg);
		out.flush();
	}

	@Override
	public void run() {
		while (true) {
			String msg = buffer.poll();
			// se c'è un messagio invio
			if (msg != null) {
				send(msg);
			}
			// altrimenti aspetto che si riempia la coda
			else {
				try {
					synchronized (buffer) {
						buffer.wait();
						// bisogna possedere l'oggetto su cui si
						// crea
						// la wait, bisogna usare il blocco
						// synchronized sull'oggetto
					}
				} catch (InterruptedException e) {
					System.err.println("Cannot wait on the queue");
				}
			}
		}
	}

	public void dispatchMessage(String msg) {
		buffer.add(msg);
		synchronized(buffer){
			buffer.notify();
		}
	}
}
